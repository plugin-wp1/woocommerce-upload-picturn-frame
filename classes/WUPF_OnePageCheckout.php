<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('WUPF_OnePageCheckout')) {

    class WUPF_OnePageCheckout
    {
        protected $nonce_action = 'wupf_one_page_checkout';
        protected $shortcode_page_id = 0;
        protected $add_scripts = false;
        protected $guest_checkout_option_changed = false;
        protected $plugin_url;
        protected $plugin_path;
        protected $template_path;

        public function __construct()
        {
            $this->plugin_path    = untrailingslashit(WUPF_PLUGIN_PATH);
            $this->template_path  = $this->plugin_path . '/templates/';

            // Filter is_checkout() on OPC posts/pages
            add_filter('woocommerce_is_checkout', array($this, 'is_checkout_filter'));

            // Display order review template even when cart is empty in WC < 2.3
            add_action('wp_ajax_woocommerce_update_order_review', array($this, 'short_circuit_ajax_update_order_review'), 9);
            add_action('wp_ajax_nopriv_woocommerce_update_order_review', array($this, 'short_circuit_ajax_update_order_review'), 9);

            // Display order review template even when cart is empty in WC 2.3+
            add_action('woocommerce_update_order_review_fragments', array($this, 'wupf_update_order_review_fragments'), 9);

            // Override the checkout template on OPC pages and Ajax requests to update checkout on OPC pages
            add_filter('wc_get_template', array($this, 'override_checkout_template'), 10, 5);

            // Ensure we have a session when loading OPC pages
            add_action('template_redirect', array($this, 'maybe_set_session'), 1);
        }


        /**
         * Function that devs can use to check if a page includes the OPC shortcode
         *
         * @since 1.1
         */
        protected function is_wupf_checkout($post_id = null)
        {
            // If no post_id specified try getting the post_id
            if (empty($post_id)) {
                global $post;

                if (is_object($post)) {
                    $post_id = $post->ID;
                } else {
                    // Try to get the post ID from the URL in case this function is called before init
                    $schema = is_ssl() ? 'https://' : 'http://';
                    $url = explode('?', $schema . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
                    $post_id = url_to_postid($url[0]);
                }
            }

            // If still no post_id return straight away
            if (empty($post_id) || is_admin()) {

                $is_opc = false;
            } else {

                if (0 == $this->shortcode_page_id) {
                    $post_to_check = !empty($post) ? $post : get_post($post_id);
                    $this->check_for_shortcode($post_to_check);
                }

                // Compare IDs
                if ($post_id == $this->shortcode_page_id || ('yes' == get_post_meta($post_id, '_wupf', true))) {
                    $is_opc = true;
                } else {
                    $is_opc = false;
                }
            }

            return apply_filters('is_wupf_checkout', $is_opc);
        }


        /**
         * Checks if any post about to be displayed contains the one page checkout shortcode.
         */
        public function check_for_shortcode($post_to_check)
        {
            if (false !== stripos($post_to_check->post_content, '['.WUPF_SHORTCODE)) {
                $this->add_scripts = true;
                $this->shortcode_page_id = $post_to_check->ID;
                $contains_shortcode = true;
            } else {
                $contains_shortcode = false;
            }

            return $contains_shortcode;
        }

        /**
         * Filter the result of `is_checkout()` for OPC posts/pages
         *
         * @param  boolean  $return
         * @return boolean
         */
        public function is_checkout_filter($return = false)
        {
            if ($this->is_wupf_checkout()) {
                $return = true;
            }

            return $return;
        }

        /**
         * Check if the installed version of WooCommerce is older than 2.3.
         *
         * @since 1.2.4
         */
        public function is_woocommerce_pre($version)
        {

            if (!defined('WC_VERSION') || version_compare(WC_VERSION, $version, '<')) {
                $woocommerce_is_pre = true;
            } else {
                $woocommerce_is_pre = false;
            }

            return $woocommerce_is_pre;
        }

        /**
         * Runs just before @see woocommerce_ajax_update_order_review() and terminates the current request if
         * the cart is empty to prevent WooCommerce printing an error that doesn't apply on one page checkout purchases.
         *
         * @since 1.0
         */
        public function short_circuit_ajax_update_order_review()
        {

            if ($this->is_woocommerce_pre('2.3') && sizeof(WC()->cart->get_cart()) == 0) {
                if (version_compare(WC_VERSION, '2.2.9', '>=')) {
                    ob_start();
                    do_action('woocommerce_checkout_order_review', true);
                    $woocommerce_checkout_order_review = ob_get_clean();

                    // Get messages if reload checkout is not true
                    $messages = '';
                    if (!isset(WC()->session->reload_checkout)) {
                        ob_start();
                        wc_print_notices();
                        $messages = ob_get_clean();

                        // Wrap messages if not empty
                        if (!empty($messages)) {
                            $messages = '<div class="woocommerce-error-ajax">' . $messages . '</div>';
                        }
                    }

                    // Setup data
                    $data = array(
                        'result'   => empty($messages) ? 'success' : 'failure',
                        'messages' => $messages,
                        'html'     => $woocommerce_checkout_order_review
                    );

                    // Send JSON
                    wp_send_json($data);
                } else {
                    do_action('woocommerce_checkout_order_review', true); // Display review order table
                    die();
                }
            }
        }

        /**
         * Set empty order review and payment fields when updating the order table via Ajax and the cart is empty.
         */
        public function wupf_update_order_review_fragments($fragments)
        {

            // If the cart is empty
            if ($this->is_any_form_of_opc_page() && 0 == sizeof(WC()->cart->get_cart())) {

                // Remove the "session has expired" notice
                if (isset($fragments['form.woocommerce-checkout'])) {
                    unset($fragments['form.woocommerce-checkout']);
                }

                $checkout = WC()->checkout();

                // To have control over when the create account fields are displayed - we'll display them all the time and hide/show with js
                if (!is_user_logged_in()) {
                    if (false === $checkout->enable_guest_checkout) {
                        $checkout->enable_guest_checkout = true;
                        $this->guest_checkout_option_changed = true;
                    }
                }

                // Add non-blocked order review fragment
                ob_start();
                woocommerce_order_review();
                $fragments['.woocommerce-checkout-review-order-table'] = ob_get_clean();

                // Reset guest checkout option
                if (true === $this->guest_checkout_option_changed) {
                    $checkout->enable_guest_checkout = false;
                    $this->guest_checkout_option_changed = false;
                }

                // Add non-blocked checkout payment fragement
                ob_start();
                woocommerce_checkout_payment();
                $fragments['.woocommerce-checkout-payment'] = ob_get_clean();
            }

            return $fragments;
        }

        /**
         * The master check for an OPC request. Checks everything from page ID to $_POST data for
         * some indication that the current request relates to an Ajax request.
         *
         * @return bool
         */
        public function is_any_form_of_opc_page()
        {

            $is_opc = false;

            // Modify template if the page being loaded (non-ajax) is an OPC page
            if ($this->is_wupf_checkout()) {

                $is_opc = true;

                // Modify template when doing a 'woocommerce_update_order_review' ajax request
            } elseif (isset($_POST['post_data'])) {

                parse_str($_POST['post_data'], $checkout_post_data);

                if (isset($checkout_post_data['is_opc'])) {
                    $is_opc = true;
                }

                // Modify template when doing ajax and sending an OPC request
            } elseif (check_ajax_referer($this->nonce_action, 'nonce', false)) {

                $is_opc = true;
            }

            return $is_opc;
        }


        /**
         * Hook to wc_get_template() and override the checkout template used on OPC pages and when updating the order review fields
         * via WC_Ajax::update_order_review()
         *
         * @return string
         */
        public function override_checkout_template($located, $template_name, $args, $template_path, $default_path)
        {
            if ($default_path !== $this->template_path && !$this->is_woocommerce_pre('2.3') && $this->is_any_form_of_opc_page()) {

                if ('checkout/form-checkout.php' == $template_name) {
                    $located = wc_locate_template('checkout/form-checkout-opc.php', '', $this->template_path);
                }
                // if ('checkout/review-order.php' == $template_name) {
                //     $located = wc_locate_template('checkout/review-order-opc.php', '', $this->$template_path);
                // }
            }

            return $located;
        }

        /**
         * Make sure a session is set whenever loading an OPC page.
         */
        public function maybe_set_session()
        {
            if ($this->is_wupf_checkout() && !WC()->session->has_session()) {
                WC()->session->set_customer_session_cookie(true);
            }
        }
    }
}
