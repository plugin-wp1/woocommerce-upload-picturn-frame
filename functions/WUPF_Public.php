<?php

/**
 * WUPF class frontend
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('WUPF_Public')) {
    class WUPF_Public extends WUPF_OnePageCheckout
    {
        protected static $wupf_product_id;
        protected static $wupf_cart_item_key;

        public function __construct()
        {
            parent::__construct();

            add_action('wp_enqueue_scripts', array($this, 'wupf_load_resources'));
            add_shortcode(WUPF_SHORTCODE, array($this, 'wupf_upload_picturn_frame_shortcode'));
            // hook action update products to cart
            add_action('wp_ajax_wupf_update_to_cart', array($this, 'wupf_update_to_cart'));
            add_action('wp_ajax_nopriv_wupf_update_to_cart', array($this, 'wupf_update_to_cart'));
            // hook action save frame picturn when order
            add_action('wp_ajax_wupf_update_frame_picturn_order', array($this, 'wupf_update_frame_picturn_order'));
            add_action('wp_ajax_nopriv_wupf_update_frame_picturn_order', array($this, 'wupf_update_frame_picturn_order'));
            // hook action before checkout create order line item
            add_action('woocommerce_checkout_create_order_line_item', array($this, 'wupf_woocommerce_checkout_create_order_line_item'), 10, 4);
        }

        // function load wp_enqueue_scripts
        public function wupf_load_resources()
        {
            // jquery
            wp_enqueue_script('wupf_jquery_js', WUPF_PLUGIN_URL . 'libs/jquery/js/jquery-3.5.1.min.js', array(), '3.5.1', true);

            wp_localize_script(
                'wupf_jquery_js',
                'wupf_infos',
                array(
                    'ajax_url' => admin_url('admin-ajax.php'),
                    'home_url' => home_url()
                )
            );
        }

        // function shortcode upload picturn frame
        public function wupf_upload_picturn_frame_shortcode($atts)
        {
            if (is_admin()) {
                return false;
            }
            
            $options = shortcode_atts(array(
                'product_id' => '',
            ), $atts);

            if ($options['product_id']) {
                self::$wupf_product_id = $options['product_id'];

                // add product to cart when cart empty
                if (WC()->cart && WC()->cart->get_cart_contents_count() == 0) {
                    if (!WC()->session->has_session()) {
                        WC()->session->set_customer_session_cookie(true);
                    }
                    WC()->cart->add_to_cart($options['product_id'], 1, 0, []);
                }

                // main style, script
                wp_enqueue_style('wupf_css', WUPF_PLUGIN_URL . 'assets/css/fe_style.css', array(), time(), 'all');
                wp_enqueue_script('wupf_js', WUPF_PLUGIN_URL . 'assets/js/fe_script.js', array(), time(), true);

                // croppie-master lib
                wp_enqueue_style('wupf_croppie_css', WUPF_PLUGIN_URL . 'libs/croppie-master/css/croppie.css', array(), '2.6.5', 'all');
                wp_enqueue_script('wupf_croppie_js', WUPF_PLUGIN_URL . 'libs/croppie-master/js/croppie.js', array(), '2.6.5', true);

                // inclue view upload picturn frame
                echo include(WUPF_PLUGIN_PATH . 'views/frontend/wupf_index.php');
            }
        }

        // function ajax update cart
        public function wupf_update_to_cart()
        {
            if (!(isset($_REQUEST['action']) || 'wupf_update_to_cart' != $_POST['action']))
                return;

            $return = array(
                'status' => false
            );

            // get product info
            $product_id = $_POST['product_id'];
            $tile_frame = $_POST['tile_frame'];
            $product_qty = $_POST['qty'];

            if ($product_id && $tile_frame && $product_qty) {

                // clear old cart
                WC()->cart->empty_cart();

                // add tile frame to cart 
                $variations_vals = ['Tile frame' => $tile_frame];

                if (!session_id()) {
                    session_start();
                }

                if (!WC()->session->has_session()) {
                    WC()->session->set_customer_session_cookie(true);
                }

                WC()->cart->add_to_cart($product_id, $product_qty, 0, $variations_vals);

                $return = array(
                    'status' => true
                );
            }

            echo json_encode($return);
            exit;
        }

        // function ajax save frame picturn when order
        public function wupf_update_frame_picturn_order()
        {
            $cart_item_key = '';
            foreach (WC()->cart->get_cart() as $key => $cart_item) {
                $cart_item_key = $key;
                break;
            }

            if (!(isset($_REQUEST['action']) || 'wupf_update_frame_picturn_order' != $_POST['action']))
                return;

            $return = array(
                'status' => false
            );

            $frame_picturn = $_POST['frame_picturn'];
            $picturn_number = $_POST['picturn_number'];

            if ($frame_picturn) {

                list($type, $frame_picturn) = explode(';', $frame_picturn);
                list(, $frame_picturn)      = explode(',', $frame_picturn);
                $frame_picturn = base64_decode($frame_picturn);

                // create directory
                if (!is_dir(WUPF_PLUGIN_PATH . 'storage/' . $cart_item_key)) {
                    // dir doesn't exist, make it
                    mkdir(WUPF_PLUGIN_PATH . 'storage/' . $cart_item_key, 0755, true);
                }
                // save picturn
                file_put_contents(WUPF_PLUGIN_PATH . 'storage/' . $cart_item_key . '/picturn_' . $picturn_number . '.png', $frame_picturn);

                $return = array(
                    'status' => true
                );

                self::$wupf_cart_item_key = $cart_item_key;
            }

            echo json_encode($return);
            exit;
        }

        // function action before checkout create order line item (woocommerce_checkout_create_order_line_item)
        public function wupf_woocommerce_checkout_create_order_line_item($item, $cart_item_key, $values, $order)
        {
            if (is_dir(WUPF_PLUGIN_PATH . 'storage/' . $cart_item_key)) {
                // item product qty
                $item_qty = $values['quantity'];

                for ($i = 0; $i < $item_qty; $i++) {
                    if (file_exists(WUPF_PLUGIN_PATH . 'storage/' . $cart_item_key . '/picturn_' . $i . '.png')) {
                        $picturn_link = WUPF_PLUGIN_URL . 'storage/' . $cart_item_key . '/picturn_' . $i . '.png';
                        // html item meta frame picturn
                        $img_html = '<a download="Frame_picturn_'.$i.'.png" href="'.$picturn_link.'" title="Frame picturn '.$i.'">
                                        <img style="width: 100px;" alt="Frame picturn '.$i.'" src="'.$picturn_link.'">
                                    </a>';
                        // add frame picturn link to order
                        $item->add_meta_data('Frame picturn ' . $i, $img_html, true);
                    }
                }
            }
        }
    }

    new WUPF_Public();
}
