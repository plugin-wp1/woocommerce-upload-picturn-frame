<?php

/**
 * @package woocommerce-upload-picturn-frame
 * 
 * Plugin Name: Woocommerce Upload Picturn Frame
 * Plugin URI:
 * Description: Woocommerce Upload Picturn Frame
 * Author: Duy Lee
 * Version:    1.0.0
 * Author URI:
 * Text Domain: wupf
 * Domain Path: /languages
 */
if (!defined('ABSPATH')) {
    exit;
}

$wupf_activated = false;

/**
 * Checking if WooCommerce is active or not for multisites
 **/
if (function_exists('is_multisite') && is_multisite()) {

    include_once(ABSPATH . 'wp-admin/includes/plugin.php');

    if (is_plugin_active('woocommerce/woocommerce.php')) {

        $wupf_activated = true;
    }
}
/**
 * Checking if WooCommerce is active
 **/
elseif (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

    $wupf_activated = true;
}

/**
 * Woocommerce is activated
 */
if ($wupf_activated) {

    define('WUPF_VERSION', '1.0.0');
    define('WUPF_NAME', 'woocommerce-upload-picturn-frame');
    define('WUPF_PLUGIN_URL', plugin_dir_url(__FILE__));
    define('WUPF_PLUGIN_PATH', plugin_dir_path(__FILE__));
    define('WUPF_PLUGIN_BASENAME', plugin_basename(__FILE__));
    define('WUPF_PAGE_LINK', 'woocommerce-upload-picturn-frame');
    define('WUPF_SHORTCODE', 'wupf_upload_picturn_frame');

    // require class
    require_once( 'classes/WUPF_OnePageCheckout.php' );

    if (is_admin()) {
    } else {
    }
    require_once WUPF_PLUGIN_PATH . 'functions/WUPF_Public.php';
    
} else {

    /**
     * Show warning message if woocommerce is not install
     */
    function wupf_plugin_error_notice()
    {
?>
        <div class="error notice is-dismissible">
            <p><?php _e('WooCommerce is not activated, Please activate WooCommerce first to install Woocommerce Upload Picturn Frame.', 'woocommerce'); ?></p>
        </div>
<?php
    }
    add_action('admin_init', 'wupf_plugin_deactivate');

    /**
     * Call Admin notices
     */
    function wupf_plugin_deactivate()
    {
        deactivate_plugins(plugin_basename(__FILE__));
        add_action('admin_notices', 'wupf_plugin_error_notice');
    }
}
