<?php
if (!defined('ABSPATH')) {
    exit;
}

// get tile frame product id woocommerce
$wupf_product_id = self::$wupf_product_id;

ob_start();
?>

<!-- main editor frame -->
<div class="main-container">

    <!-- filter menu -->
    <div class="filter-strip">
        <div class="filter-button selected" filter_class="frame_classic">
            <div class="filter-bottom">
                <div class="filter-name">Classic</div>
            </div>
        </div>
        <div class="filter-button" filter_class="frame_bold">
            <div class="filter-bottom">
                <div class="filter-name">Bold</div>
            </div>
        </div>
        <div class="filter-button" filter_class="frame_ever">
            <div class="filter-bottom">
                <div class="filter-name">Ever</div>
            </div>
        </div>
        <div class="filter-button" filter_class="frame_clean">
            <div class="filter-bottom">
                <div class="filter-name">Clean</div>
            </div>
        </div>
        <div class="filter-button" filter_class="frame_edge">
            <div class="filter-bottom">
                <div class="filter-name">Edge</div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="TilesStrip" filter="frame_classic">

            <!-- add new image frame -->
            <div class="tile" id="tile-0" image="empty" data-index="0">
                <div class="tile_base"></div>

                <!-- button add image -->
                <div title="Add image" class="image_add">
                    <img class="icon" src="<?= WUPF_PLUGIN_URL . 'assets/images/icons/add.svg' ?>">
                </div>

                <!-- button crop image -->
                <div title="Adjust Crop" class="frameless crop">
                    <img class="icon" src="<?= WUPF_PLUGIN_URL . 'assets/images/icons/crop.svg' ?>">
                </div>
                <!-- button remove image -->
                <div title="Remove Tile" class="frameless remove">
                    <img class="icon" src="<?= WUPF_PLUGIN_URL . 'assets/images/icons/remove.svg' ?>">
                </div>

                <!-- review image -->
                <div class="preview matting">
                    <img alt="" class="preview_image" src="">
                </div>

                <!-- frames -->
                <div class="TileFrame">
                    <img alt="" class="frame fr_black_1" src="<?= WUPF_PLUGIN_URL . 'assets/images/frames/black.svg' ?>">
                    <img alt="" class="frame fr_black_2" src="<?= WUPF_PLUGIN_URL . 'assets/images/frames/black.svg' ?>">
                    <img alt="" class="frame fr_white_1" src="<?= WUPF_PLUGIN_URL . 'assets/images/frames/white.svg' ?>">
                    <img alt="" class="frame fr_white_2" src="<?= WUPF_PLUGIN_URL . 'assets/images/frames/white.svg' ?>">
                    <img alt="" class="frame fr_none" src="<?= WUPF_PLUGIN_URL . 'assets/images/frames/none.svg' ?>">
                </div>

                <!-- input image -->
                <input class="input_image" type="file" accept="image/*" hidden>

                <!-- popup resize image -->
                <div class="popup_overlay">
                    <div class="popup_container">
                        <div class="popup_head">
                            <div class="pull_right">
                                <a class="done_button" rel="modal:close">Done</a>
                            </div>
                        </div>
                        <div class="cropper_wrap">
                            <img class="crop_image" src="" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- woocommerce checkout form -->
    <div class="woo_checkout_container">
        <?php
        // Get checkout object for WC 2.0+
        $checkout = WC()->checkout();
        wc_get_template('checkout/form-checkout.php', array('checkout' => $checkout));
        ?>
    </div>

    <!-- footer checkout -->
    <div class="bottom_bar">
        <div class="checkout_wrap">
            <button class="checkout_btn"><?= __('Checkout', 'wupf') ?></button>
        </div>
    </div>

</div>

<!-- get plugin url -->
<input type="hidden" id="WUPF_PLUGIN_URL" value="<?= WUPF_PLUGIN_URL ?>">
<!-- get tile frame product id -->
<input type="hidden" id="WUPF_PRODUCT_ID" value="<?= $wupf_product_id ?>">

<?php
return ob_get_clean();
