$(document).ready(function () {

    // get plugin url
    const WUPF_PLUGIN_URL = $('#WUPF_PLUGIN_URL').val();
    // get tile frame product id
    const WUPF_PRODUCT_ID = $('#WUPF_PRODUCT_ID').val();
    // image number uploaded
    var i_img = 1;

    // function load image input to frame
    function loadImageToFrame(input, toFrame) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                toFrame.attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);

        }
    }

    // function add new tile iframe
    function addNewTileFrame() {
        var tile_html = 
            '<!-- add new image frame -->\
            <div class="tile" id="tile-'+i_img+'" image="empty" data-index="'+i_img+'">\
                <div class="tile_base"></div>\
                <!-- button add image -->\
                <div title="Add image" class="image_add">\
                    <img class="icon" src="'+WUPF_PLUGIN_URL+'/assets/images/icons/add.svg">\
                </div>\
                <!-- button crop image -->\
                <div title="Adjust Crop" class="frameless crop">\
                    <img class="icon" src="'+WUPF_PLUGIN_URL+'/assets/images/icons/crop.svg">\
                </div>\
                <!-- button remove image -->\
                <div title="Remove Tile" class="frameless remove">\
                    <img class="icon" src="'+WUPF_PLUGIN_URL+'/assets/images/icons/remove.svg">\
                </div>\
                <!-- review image -->\
                <div class="preview matting">\
                    <img alt="" class="preview_image" src="">\
                </div>\
                <!-- frames -->\
                <div class="TileFrame">\
                    <img alt="" class="frame fr_black_1" src="'+WUPF_PLUGIN_URL+'/assets/images/frames/black.svg">\
                    <img alt="" class="frame fr_black_2" src="'+WUPF_PLUGIN_URL+'/assets/images/frames/black.svg">\
                    <img alt="" class="frame fr_white_1" src="'+WUPF_PLUGIN_URL+'/assets/images/frames/white.svg">\
                    <img alt="" class="frame fr_white_2" src="'+WUPF_PLUGIN_URL+'/assets/images/frames/white.svg">\
                    <img alt="" class="frame fr_none" src="'+WUPF_PLUGIN_URL+'/assets/images/frames/none.svg">\
                </div>\
                <!-- input image -->\
                <input class="input_image" type="file" accept="image/*" hidden>\
                <!-- popup resize image -->\
                <div class="popup_overlay">\
                    <div class="popup_container">\
                        <div class="popup_head">\
                            <div class="pull_right">\
                                <a class="done_button" rel="modal:close">Done</a>\
                            </div>\
                        </div>\
                        <div class="cropper_wrap">\
                            <img class="crop_image" src="" />\
                        </div>\
                    </div>\
                </div>\
            </div>';

            $('.TilesStrip').append(tile_html);
            i_img++;
    }

    // function update product to cart
    function updateProductCart() {
        var prod_qty = $('.tile.active').length;
        var tile_frame = $('.filter-strip .filter-button.selected .filter-name').text();
        if(prod_qty > 0 && tile_frame) {

            var info = {};
            info['action'] = 'wupf_update_to_cart';
            info['product_id'] = WUPF_PRODUCT_ID;
            info['tile_frame'] = tile_frame;
            info['qty'] = prod_qty;

            //ajax update cart
            $.post(wupf_infos.ajax_url, info).done(function (data) {
                data = JSON.parse(data);
                if( data.status ) {
                    $(document.body).trigger("update_checkout");
                }
            });
        }
    }

    // function save frame picturn order
    function saveFramePicturn() {

        var result = false;
        $('.tile.active').each(async function (index, element) {

            var frame_picturn = $(this).find('.preview .preview_image').attr('src');
            if(frame_picturn) {

                $.ajax({
                    type: 'POST',
                    url: wupf_infos.ajax_url,
                    async: false,
                    data: {
                        action: 'wupf_update_frame_picturn_order',
                        picturn_number: index,
                        frame_picturn: frame_picturn
                    },
                    success: function(res) {
                        res = JSON.parse(res);
                        result = res.status;
                        if(!result) {
                            return false;
                        }
                    } 
                });
            }
        });

        return result;
    }

    // select filter
    $('.filter-strip .filter-button').click(function (e) {
        $('.filter-strip .filter-button').removeClass('selected');
        $(this).addClass('selected');

        var filter_class = $(this).attr('filter_class');
        // add filter to frame
        $('.TilesStrip').attr('filter', filter_class);

        // ajax update product cart
        updateProductCart();
    });

    // load image to tile frame
    $(document).on('change', '.input_image', function (e) {
        if ($(this)[0].files.length !== 0) {
            var tileParent = $(this).parents('.tile');

            // add has image class to tile
            tileParent.addClass('active').attr('image', 'exist');
            // add new tile frame
            addNewTileFrame();

            // add input image to review image
            reviewImage = tileParent.find('.preview .preview_image');
            loadImageToFrame(this, reviewImage);

            // add input image to popup crop image
            cropImage = tileParent.find('.cropper_wrap .crop_image');
            loadImageToFrame(this, cropImage);

            // ajax update product cart
            updateProductCart();
        }
    });

    // add new image
    $(document).on('click', '.tile .image_add', function (e) {
        e.preventDefault();
        var inputImg = $(this).parents('.tile').find('.input_image');
        if (inputImg[0].files.length === 0) {
            inputImg.click();
        }
    });

    // crop image
    var uploadedCrop = Array();
    $(document).on('click', '.frameless.crop', function (e) {
        // check exist image
        if ($(this).parents('.tile').find('.input_image')[0].files.length !== 0) {
            // popup crop image
            var popupCrop = $(this).parents('.tile').find('.popup_overlay');
            // tile index
            var tile_index = $(this).parents('.tile').attr('data-index');


            // show popup
            popupCrop.show();
            // Cropper image
            if (!$(this).hasClass('loaded')) {
                uploadedCrop[tile_index] = popupCrop.find('.crop_image').croppie({
                    viewport: {
                        width: 200,
                        height: 200
                    }
                });
                $(this).addClass('loaded');
            }
        } else {
            window.alert('please input image!');
        }
    });

    // remove tile frame
    $(document).on('click', '.frameless.remove', function (e) {
        $(this).parents('.tile').remove();

        // ajax update product cart
        updateProductCart();
    });

    // done crop popup
    $(document).on('click', '.popup_overlay .done_button', function (e) {

        var tileParent = $(this).parents('.tile');
        var tile_index = tileParent.attr('data-index');

        uploadedCrop[tile_index].croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (resp) {
            tileParent.find('.preview .preview_image').attr('src', resp);
        });
    });

    // show woocommerce form checkout
    $(document).on('click', '.bottom_bar .checkout_btn', function (e) {
        if($('.tile.active').length) {
            // show checkout form
            $('.woo_checkout_container').show();
            // hidden bottom
            $('.bottom_bar').hide();
            // scroll to checkout form
            $('html, body').animate({
                scrollTop: $(".woo_checkout_container").offset().top
            }, 800);
        } else {
            window.alert('please input picturn');
        }
    });

    // ajax save frame picturn when order
    $(document).on('click', '#wupf_place_order', function (e) {
        // e.preventDefault();

        // add animation loading checkout form
        $('.woo_checkout_container').addClass('wupf_loading');
        // ajax save picturn
        var savePicturn = saveFramePicturn();
        
        if(savePicturn) {
            // remove animation loading
            $('.woo_checkout_container').removeClass('wupf_loading');
            // submit checkout form
            $('form.woocommerce-checkout').submit();
        }
    });

    // POPUP lib----------------------------------------------------------->
    // click overlay close popup
    $(document).on('click', '.popup_overlay', function (e) {
        $(this).hide();
    });
    // button open
    $(document).on('click', '*[data-toggle~="modal"]', function (e) {
        $($(this).attr('data-target')).show();
    });
    // button close
    $(document).on('click', 'a[rel~="modal:close"]', function (e) {
        $(this).parents('.popup_overlay').hide();
    });
    $(document).on('click', '.popup_container', function (e) {
        e.stopPropagation();
    });
    
});